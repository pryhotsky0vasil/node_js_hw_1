const express = require("express");
const router = express.Router();
const {
  createFile,
  getFiles,
  getFile,
  deleteFile,
  editFile,
} = require("./filesService.js");

router.post("/", createFile);

router.get("/", getFiles);

router.get("/:filename", getFile);

router.delete("/:filename", deleteFile);

router.put("/", editFile);

// Other endpoints - put, delete.

module.exports = {
  filesRouter: router,
};
