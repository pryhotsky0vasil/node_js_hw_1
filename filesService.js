const fs = require("fs");
const Path = require("path");

const ext = [".log", ".txt", ".json", ".yaml", ".xml", ".js"];
const extValidate = (file) => ext.includes(Path.parse(file).ext);

const createFile = (req, res, next) => {
  const { filename, content } = req.body;
  const way = Path.resolve("files", filename);

  try {
    if (!filename) {
      throw new Error("Invalid file name");
    }
    if (!extValidate(filename)) {
      throw new Error("Invalid extension");
    }
    if (!content) {
      throw new Error("Invalid content");
    }
    fs.writeFile(way, content, (err) => {
      if (err) throw err;
      res
        .status(200)
        .send({ message: `File ${filename} created successfully` });
    });
  } catch (err) {
    res.status(400).send({ message: err.message });
  }
};

const getFiles = (req, res, next) => {
  const way = "files/";

  try {
    fs.readdir(way, (err, files) => {
      if (err) throw err;
      res.status(200).send({
        message: "Success",
        files: files,
      });
    });
  } catch (err) {
    res.status(400).send({ message: err.message });
  }
};

const getFile = (req, res, next) => {
  const filename = req.params.filename;
  const extension = Path.extname(filename).slice(1);
  const way = Path.resolve("files", filename);

  try {
    if (!fs.existsSync(way)) {
      throw new Error(`File ${filename} not found`);
    }
    fs.readFile(way, "utf-8", (err, data) => {
      if (err) throw err;
      res.status(200).send({
        message: "Success",
        filename: filename,
        content: data,
        extension: extension,
        uploadedDate: fs.statSync(way).birthtime,
      });
    });
  } catch (err) {
    res.status(400).send({ message: err.message });
  }
};

const deleteFile = (req, res, next) => {
  const filename = req.params.filename;
  const way = Path.resolve("files", filename);

  try {
    fs.unlink(way, (err) => {
      if (err) throw err;
      res
        .status(200)
        .send({ message: `File ${filename} deleted successfully` });
    });
  } catch (err) {
    res.status(400).send({ message: err.message });
  }
};

const editFile = (req, res, next) => {
  const { filename, content } = req.body;
  const way = Path.resolve("files", filename);

  try {
    fs.writeFile(way, content, "a", (err) => {
      if (err) throw err;
      res.status(200).send({ massage: `File ${filename} update successfully` });
    });
  } catch (err) {
    res.status(400).send({ message: err.message });
  }
};

module.exports = {
  createFile,
  getFiles,
  getFile,
  deleteFile,
  editFile,
};
